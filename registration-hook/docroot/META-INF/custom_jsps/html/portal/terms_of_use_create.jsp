<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/html/portal/init.jsp" %>

<%
String currentURL = PortalUtil.getCurrentURL(request);
// If the page comes from home:
//	currentURL:/en_GB/?p_p_id=58&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&saveLastPath=0&_58_struts_action=%2Flogin%2Fcreate_account
//If comes from languaje selection
//currentURL:/overlay?p_p_id=58&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&saveLastPath=0&_58_struts_action=%2Flogin%2Fcreate_account
String sLanguage = locale.getLanguage();
String urllenguage="";
if (sLanguage.equals("es")){
	urllenguage = "es_ES/";				
}
else if (sLanguage.equals("en")){
	urllenguage = "en_GB/";				
}
else if (sLanguage.equals("it")){
	urllenguage = "it_IT/";				
}
else if (sLanguage.equals("fi")){
	urllenguage = "fi_FI/";				
}
else if (sLanguage.equals("sr")){//sr_RS_latin //sr_RS //sh
	urllenguage = "sr_RS_latin/";				
}
else if (sLanguage.equals("sh")){//sr_RS_latin //sr_RS //sh
	urllenguage = "sr_RS/";				
}
else{
	urllenguage = "en_GB/";
}
currentURL = currentURL.replace("overlay", urllenguage);

int qPos = currentURL.indexOf('?');
int hPos = currentURL.indexOf('#');
char separator = qPos == -1 ? '?' : '&';
String paramName = String.format("%s%s", portletDisplay.getNamespace(), "terms_agree");
String param = String.format("%s%s=%s", separator, paramName, "true");
String agreeURL = hPos == -1 ? currentURL + param : currentURL.substring(0, hPos) + separator + currentURL.substring(hPos);

//agreeURL:/en_GB/?p_p_id=58&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&saveLastPath=0&_58_struts_action=%2Flogin%2Fcreate_account&_58_terms_agree=true
//agreeURL:/overlay?p_p_id=58&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&saveLastPath=0&_58_struts_action=%2Flogin%2Fcreate_account&_58_terms_agree=true
%>

<%@ include file="/html/portal/terms_of_use.jspf" %>
<script>
var currentURL="<%=currentURL %>";
var agreeURL="<%=agreeURL %>";
console.log('currentURL:'+currentURL);
console.log('agreeURL:'+agreeURL);
</script>
<aui:button-row>
	<%
	String agreeOnClick = String.format("window.location.href='%s'", agreeURL);
	String taglibOnClick = "alert('" + UnicodeLanguageUtil.get(pageContext, "you-must-agree-with-the-terms-of-use-to-continue") + "');";
	%>
	<div id="materialize-body" class="materialize">
	<aui:button onClick="<%= taglibOnClick %>" cssClass="red lighten-1 white-text btn btn-flat" type="cancel" value="i-disagree" />
	<aui:button onClick="<%= agreeOnClick %>"  cssClass="green lighten-1 white-text btn btn-flat" type="submit" value="i-agree" />

	</div>
</aui:button-row>

<link rel="stylesheet" type="text/css" href="/html/css/fix.css">

