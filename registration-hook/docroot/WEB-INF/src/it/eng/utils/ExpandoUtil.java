package it.eng.utils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portlet.expando.DuplicateColumnNameException;
import com.liferay.portlet.expando.DuplicateTableNameException;
import com.liferay.portlet.expando.model.ExpandoColumn;
import com.liferay.portlet.expando.model.ExpandoTable;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.service.ExpandoColumnLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoTableLocalServiceUtil;

/**
 * 
 * Currently unused.
 * 
 * @author eneko.gomez@tecnalia.com
 *
 */
public abstract class ExpandoUtil {
	public static ExpandoColumn addExpandoAttribute(Long companyId,
			String columnName, int type, String className)
			throws PortalException, SystemException {

		ExpandoTable table = null;
		ExpandoColumn expandoColumn = null;

		if (companyId == null) {
			companyId = CompanyLocalServiceUtil.getCompanies().get(0)
					.getCompanyId();
		}

		try {
			table = ExpandoTableLocalServiceUtil.addTable(companyId, className,
					ExpandoTableConstants.DEFAULT_TABLE_NAME);
		} catch (DuplicateTableNameException dtne) {
			table = ExpandoTableLocalServiceUtil.getTable(companyId, className,
					ExpandoTableConstants.DEFAULT_TABLE_NAME);
		}

		try {
			expandoColumn = ExpandoColumnLocalServiceUtil.addColumn(
					table.getTableId(), columnName, type);
		} catch (DuplicateColumnNameException dcne) {
			expandoColumn = ExpandoColumnLocalServiceUtil.getColumn(companyId,
					className, ExpandoTableConstants.DEFAULT_TABLE_NAME,
					columnName);
		}
		
		return expandoColumn;
	}

	public static ExpandoColumn addExpandoAttribute(String columnName,
			int type, String className) throws PortalException, SystemException {
		return ExpandoUtil.addExpandoAttribute((Long) null, columnName, type,
				className);
	}

	public static ExpandoColumn addExpandoAttribute(Long companyId,
			String columnName, int type, Class clazz) throws PortalException,
			SystemException {
		String className = clazz.getName();
		return ExpandoUtil.addExpandoAttribute(companyId, columnName, type,
				className);
	}

	public static ExpandoColumn addExpandoAttribute(String columnName,
			int type, Class clazz) throws PortalException, SystemException {
		String className = clazz.getName();
		return ExpandoUtil.addExpandoAttribute(columnName, type, className);
	}
}
