package it.eng.utils;

import java.util.HashMap;
import java.util.Map;

import com.liferay.portal.kernel.util.Validator;

public class Utils {

	/**
	 * @param errorCode
	 * @return
	 */
	public static String getErrorDescriptionByCode(int errorCode){
		
		
		Map<Integer, String> mapping = new HashMap<Integer, String>();
		
		mapping.put( Constants.CODE_ACTION_SUCCESS , "ACTION_SUCCESS");
		mapping.put( Constants.CODE_ACTION_FAILED , "ACTION_FAILED");
		mapping.put( Constants.CODE_CCUSERID_NOT_FOUND , "CCUSERID_NOT_FOUND");
		mapping.put( Constants.CODE_INPUTS_ERROR , "INPUTS_ERROR");
		mapping.put( Constants.CODE_RESOURCE_ERROR , "RESOURCE_ERROR");
		mapping.put( Constants.CODE_AUTHORIZATION_ERROR , "AUTHORIZATION_ERROR");
		
		String descr = mapping.get(errorCode);
		
		if (Validator.isNull(descr))
		 descr = "other";
		
		return descr;
		
	}	

}
